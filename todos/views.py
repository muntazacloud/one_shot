from django.shortcuts import render,redirect,  get_object_or_404
from todos.models import TodoList
from .forms import TodoForm

from django.db.models import Count


def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "todolists": lists
    }
    return render(request, "todos/index.html", context)


def todo_list_detail(request, id):
    list = get_object_or_404(TodoList, id=id)

    context = {
        "list": list,
    }

    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect('todo_list_detail',  id=todo_list.id)
    else:
        form = TodoForm()

    context = {
        "form": form
    }

    return render(request, "todos/create.html", context)
